import React from 'react'
import {connect} from 'react-redux'
import MyTable from '../components/myTable'
import {setActiveRow, changeField, activateEditMode, deactivateEditMode} from '../actions/myTableActions'

const MyTableContainer = props =>
  <MyTable {...props} />

const mapStateToProps = ({myTable}, ownProps) => ({
  ...ownProps,
  activeRowId: myTable.get('activeRow'),
  modifiedRow: myTable.get('modifiedRow')
})

const mapDispatchToProps = {
  setActiveRow,
  changeField,
  activateEditMode,
  deactivateEditMode
}

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: true })(MyTableContainer)
