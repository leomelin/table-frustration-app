import React from 'react'
import {connect} from 'react-redux'
import {save} from '../actions/appActions'
import App from '../components/app'

const AppContainer = props =>
  <App {...props} />

const mapStateToProps = (state, ownProps) => {
  return {
    users: state.app.get('users'),
    savingInProgress: state.app.get('savingInProgress')
  }
}

const mapDispatchToProps = {
  save
}

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: true })(AppContainer)
