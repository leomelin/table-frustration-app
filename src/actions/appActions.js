import {
  SAVING_IN_PROGRESS,
  SAVING_COMPLETE,
  UPDATE_USER,
  RESET_MYTABLE
} from '../constants/actionTypes'

const savingInProgress = () => dispatch => {
  dispatch({
    type: SAVING_IN_PROGRESS
  })
}

const savingComplete = () => dispatch => {
  dispatch({
    type: SAVING_COMPLETE
  })
}

const save = updatedUser => dispatch => {
  savingInProgress()(dispatch)

  // Emulate ajax request...
  setTimeout(() => {
    dispatch({
      type: UPDATE_USER,
      updatedUser
    })
    savingComplete()(dispatch)
    dispatch({
      type: RESET_MYTABLE,
      updatedUser
    })
  }, 2000)
}

export {
  savingInProgress,
  savingComplete,
  save
}
