import {
  SET_ACTIVE_ROW,
  CHANGE_FIELD,
  ACTIVATE_EDIT_MODE,
  DEACTIVATE_EDIT_MODE
} from '../constants/actionTypes'

const setActiveRow = rowIndex => dispatch => {
  dispatch({
    type: SET_ACTIVE_ROW,
    rowIndex
  })
}

const changeField = (fieldKey, value) => dispatch => {
  dispatch({
    type: CHANGE_FIELD,
    fieldKey,
    value
  })
}

const activateEditMode = row => dispatch => {
  dispatch({
    type: ACTIVATE_EDIT_MODE,
    row
  })
}

const deactivateEditMode = () => dispatch => {
  dispatch({
    type: DEACTIVATE_EDIT_MODE
  })
}

export {
  setActiveRow,
  changeField,
  activateEditMode,
  deactivateEditMode
}
