import Immutable from 'immutable'

const initialState = Immutable.fromJS({
  activeRow: null,
  modifiedRow: null
})

const myTable = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_ACTIVE_ROW':
      return state.set('activeRow', action.rowIndex)
    case 'CHANGE_FIELD':
      return state.setIn(['modifiedRow', action.fieldKey], action.value)
    case 'ACTIVATE_EDIT_MODE':
      return state.set('modifiedRow', action.row)
    case 'DEACTIVATE_EDIT_MODE':
      return state.set('modifiedRow', null)
    case 'RESET_MYTABLE':
      return state = initialState
    default:
      return state
  }
}

export default myTable
