import {combineReducers} from 'redux'
import myTable from './myTable'
import app from './app'
export default combineReducers({
  myTable,
  app
})
