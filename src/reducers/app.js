import Immutable from 'immutable'
import users from './users.json'

const initialState = Immutable.fromJS({
  savingInProgress: false,
  users
})

const app = (state = initialState, action) => {
  switch (action.type) {
    case 'UPDATE_USER':
      return state.update('users', users => users.map(user => {
        if (user.get('id') === action.updatedUser.get('id')) {
          return action.updatedUser
        }

        return user
      }))
    case 'SAVING_IN_PROGRESS':
      return state.set('savingInProgress', true)
    case 'SAVING_COMPLETE':
      return state.set('savingInProgress', false)
    default:
      return state
  }
}

export default app
