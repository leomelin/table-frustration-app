import React from 'react'
import {pure} from 'recompose'

const BtnCol = ({
  deactivateEditMode,
  editBtnDisabled,
  editModeActivated,
  activateEditMode,
  row,
  disabled,
  save
}) => {
  if (editModeActivated) {
    return (
      <td className="edit-btn-col">
        <button disabled={disabled} onClick={e => deactivateEditMode()}>Cancel</button>
        <button disabled={disabled} onClick={e => save(row)}>Save</button>
      </td>
    )
  }

  return (
    <td className="edit-btn-col">
      <button disabled={editBtnDisabled || disabled} onClick={e => activateEditMode(row)}>Edit</button>
    </td>
  )
}

export default pure(BtnCol)
