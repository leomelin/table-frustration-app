import React from 'react'
import {pure} from 'recompose'
import Col from './col'
import BtnCol from './btnCol'

const MyRow = ({
  rowIndex,
  cols,
  active,
  save,
  setActiveRow,
  activateEditMode,
  deactivateEditMode,
  editBtnDisabled,
  changeField,
  ...rest
}) => {
  console.log('Rendering row', rowIndex)
  return (
    <tr
      className={(active && 'active') || ''}
      onClick={e => setActiveRow(rowIndex)}
    >
      {cols.map((col, cindex) =>
        <Col
          key={cindex}
          col={col}
          changeField={changeField}
          {...rest}
        />
      )}
      <BtnCol
        editBntDisabled={editBtnDisabled}
        save={save}
        activateEditMode={activateEditMode}
        deactivateEditMode={deactivateEditMode}
        {...rest}
      />
    </tr>
  )
}

export default pure(MyRow)
