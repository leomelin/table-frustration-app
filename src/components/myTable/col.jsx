import React from 'react'
import {pure} from 'recompose'

const Col = ({
  row,
  col,
  editModeActivated,
  changeField,
  disabled
}) =>
  <td>
    {!col.get('readOnly') && editModeActivated &&
        <input disabled={disabled}
          type="text"
          value={row.get(col.get('fieldKey'))}
          onChange={e => changeField(col.get('fieldKey'), e.target.value)}
        />
    }
    {(col.get('readOnly') || !editModeActivated) &&
      row.get(col.get('fieldKey'))}
  </td>

export default pure(Col)
