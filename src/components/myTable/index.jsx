import React from 'react'
import {pure} from 'recompose'
import './table.css';
import MyRow from './row'

const MyTable = ({rows, rowIdKey, modifiedRow, activeRowId, ...rest}) =>
  <table className="my-table">
    <tbody>
      {rows.map((rowObj, rindex) => {
        const rowIndex = rowObj.get(rowIdKey) || rindex
        const editBtnDisabled = !!modifiedRow
        const editModeActivated = modifiedRow &&
          modifiedRow.get('id') === rowObj.get('id')
        const active = activeRowId === rowIndex

        const row = editModeActivated
          ? modifiedRow
          : rowObj

        return <MyRow
          key={rowIndex}
          active={active}
          rowIndex={rowIndex}
          row={row}
          editBtnDisabled={editBtnDisabled}
          editModeActivated={editModeActivated}
          {...rest}
        />
      })}
    </tbody>
  </table>

export default pure(MyTable)

