import React from 'react'
import MyTableContainer from '../../containers/myTableContainer'
import Immutable from 'immutable'
import {pure} from 'recompose'

const cols = Immutable.fromJS([{
  fieldKey: 'id',
  readOnly: true
}, {
  fieldKey: 'firstName'
}, {
  fieldKey: 'lastName'
}, {
  fieldKey: 'email'
}, {
  fieldKey: 'gender'
}])

const App = ({users, savingInProgress, save}) =>
  <section className="app">
    <h1>This is a table component demo</h1>
    <MyTableContainer cols={cols} rows={users} rowIdKey="id" save={save} disabled={savingInProgress} />
  </section>

export default pure(App)

