- Action types to constants... it's a good practice
- Some of the components use "connect" in a way that make reusing components hard. Need to implement container components to make it more "redux"
- oh, connect has "pure" option, I can ditch the recompose library on container components
- Some more refactoring done to make things look more "neat"

Some things to think about:
- Should I use reselect? http://jaysoo.ca/2016/02/28/organizing-redux-application/
- Should I be using redux action to handle actions ? https://github.com/acdlite/redux-actions
